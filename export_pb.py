import argparse
import requests
import pyperclip
import json
import sys
import re
import urllib3

##############################################################################################
#                               G L O B A L    V A R I A B L E S                             #
##############################################################################################

global_headers = {'content-type': 'application/json'}
global_server = ''
global_playbook_id = ''
global_bundles = ''
global_session = requests.Session()

##############################################################################################
#                               U S E R    D E F I N I T I O N S                             #
##############################################################################################

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def init_resolver():
    global global_bundles

    req_bundles = global_session.get(global_server + '/api/management/bundles', headers=global_headers)
    global_bundles = json.loads(req_bundles.text)

def id_resolve_key(id):
    for bundle_item in global_bundles['items']:
        bundle_objects = bundle_item['spec']['objects']
        
        for object in bundle_objects:
            target_app_id = object['spec']['id']

            if target_app_id == id:
                return object['spec']['key'], bundle_item['id']

    eprint('ERROR: ' + id + ' was not found!')
    return 'UNKNOWN'

def authenticate(username: str, password: str):
	login_payload = {'username': username, 'password': password}
	req_login = global_session.post(global_server + '/api/auth/login', headers=global_headers, data=json.dumps(login_payload))
	if req_login.status_code == 200: 
		global_headers['Authorization'] = f'Bearer {req_login.json()["access_token"]}'
	else:
		eprint(f'ERROR: Authentication unsuccesful: {req_login.status_code}')

def get_playbook(playbook_id: str, no_strip: bool, no_cai: bool, minify: bool):
    # Load the given playbook
    req_pb = global_session.get(global_server + '/api/management/playbooks/' + playbook_id, headers=global_headers)
    payload = req_pb.text

    # Remove unnecessary fields
    if not no_strip:
        payload = re.sub(r'("apps"\s*:\s*\[)([\s\S]*?)([ \t]*\]\s?,?\n?)', r'\1\n\3', payload)
        payload = re.sub(r'"id":\s*".*?",', '', payload)
        payload = re.sub(r'"(?:modified_at|created_at)"\s*:\s*\d+,', r'', payload)

    # Extract CAIs
    # Currently only handle:
    #   Tasks & Apps - No Instances!
    if not no_cai:
        init_resolver()

        temp_payload = payload

        for id_match in re.finditer(r'"custom\/(.*?)\/tasks\/(.*?)"', payload):
            app_key, bundle_key_a = id_resolve_key(id_match.group(1))
            task_key, bundle_key_t = id_resolve_key(id_match.group(2))

            if bundle_key_a != bundle_key_t:
                eprint('ERROR: bundle id confusion, where ' + bundle_key_a + ' != ' + bundle_key_t)

            reconstructed = '"custom/@' + bundle_key_a + '/' + app_key + '@/tasks/@' + bundle_key_a + '/' + task_key + '@"'

            temp_payload = temp_payload.replace(id_match.group(0), reconstructed)

        payload = temp_payload

    # Either minify or prettify the output
    loaded = json.loads(payload)
    if minify:
        payload = json.dumps(loaded, separators=(',', ':'))
    else:
        payload = json.dumps(loaded, indent=4)

    return payload

def scrape_playbook_ids():
    req_pb = global_session.get(global_server + '/api/management/playbooks', headers=global_headers)
    loaded = json.loads(req_pb.text)

    return [a['id'] for a in loaded['items']]


##############################################################################################
#                                M A I N    E N T R Y P O I N T                              #
##############################################################################################

parser = argparse.ArgumentParser(description='Export playbooks from OPSWAT MAS.', epilog='Internal use only!', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('playbook_id', nargs='?', default=None, help='ID of the playbook you\'d like to export.')
parser.add_argument('-s', '--srv', nargs=1, default=['localhost:3001'], help='Target MAS server to query (ip/domain:port format).')
parser.add_argument('-o', '--out', nargs=1, help='Target file to output. By default stdout.')
parser.add_argument('-c', '--clip', action='count', default=0, help='Put result on the clipboard.')
parser.add_argument('-u','--username', nargs=1, default=['administrator'], help='Name of the user whose privileges to be utilized')
parser.add_argument('-p','--password', nargs=1, default=['administrator'], help='Password of the user whose privileges to be utilized')
parser.add_argument('-m', '--minify', action='count', default=0, help='Return minified json instead of pretty result.')
parser.add_argument('--http', action='count', default=0, help='Use HTTP instead of HTTPS.')
parser.add_argument('--no-strip', action='count', default=0, help='Don\'t strip values which are changing between installations, such as: apps, playbook id, dates.')
parser.add_argument('--no-cai', action='count', default=0, help='Don\'t replace custom applications with their bundle macros.')

args = parser.parse_args()

# HTTP(S) related settings & build the connection string
if not args.srv[0].startswith('http'):
    global_server = ('http' if args.http else 'https') + '://' + args.srv[0]
elif args.srv[0].startswith('http') and not args.http:
    global_server = args.srv[0]
    print('URL contains HTTP directive, ignoring --http option.')

if global_server.startswith('https'): global_session.verify = False
if global_server.startswith('https'): urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# Log in to have a valid session
authenticate(args.username[0], args.password[0])

out_filename_without_ext = ''
if args.out:
    if str(args.out[0]).endswith('.json'):
        out_filename_without_ext = args.out[0][:-5]
    else:
        out_filename_without_ext = args.out[0]

if args.playbook_id:
    payload = get_playbook(args.playbook_id, args.no_strip, args.no_cai, args.minify)

    # Write to File or to STDOUT
    if args.out:
        f = open(f'{out_filename_without_ext}.json', 'w')
        f.write(payload)
        f.close()
        print(f'Exported "{out_filename_without_ext}.json" successfully.')
    else:
        print(payload)

    # Copy to clipboard if asked for
    if args.clip:
        pyperclip.copy(payload)
else:
    pbs = scrape_playbook_ids()

    for playbook_id in pbs:
        payload = get_playbook(playbook_id, args.no_strip, args.no_cai, args.minify)
        if args.out:
            f = open(f'{out_filename_without_ext}-{playbook_id}.json', 'w')
            f.write(payload)
            f.close()
            print(f'Exported "{out_filename_without_ext}-{playbook_id}.json" successfully.')
        else:
            print(payload)
            print()
