# MAS Tools

These tools are currently only used internally!

---

## Install

In order to be able to run scripts you'll need `Python3` and `pip`.
If you need help with the installation of Python, have a look at this guide: https://phoenixnap.com/kb/how-to-install-python-3-windows

You will also need to have `git` installed. In case of Windows you can find it here: https://gitforwindows.org/


To clone the scripts & install prerequisites, run:

```bash
git clone git@bitbucket.org:RolandGyorffi/mas-tools.git
cd mas-tools
pip install -r requirements.txt
```

---

## Update

If you already have the scripts cloned and there's a new update, you'll have to do the following:

```bash
cd <mas-tools directory>
git pull --rebase
pip install -r requirements.txt
```

---

## Notes

By default each script tries to query a LOCALHOST mas server! So if you need something else, like mas-demo:3001 or anything else remote, then you have to specify the --srv (or -s) switch! Check below for usage.

From now, each script accepts BOTH:
  - http://localhost:3001/
  - localhost:3001

formats as a valid server input.

---

## Script: `export_pb.py`

Used to export MAS Playbooks into a JSON which can be later imported or manually edited. By default it parses CAIs elements into the format of a "Bundle" so that requires no extra manual step.

```
usage: export_pb.py [-h] [-s SRV] [-o OUT] [-c] [-u USERNAME] [-p PASSWORD] [-m] [--http] [--no-strip] [--no-cai] playbook_id

Export playbooks from OPSWAT MAS.

positional arguments:
  playbook_id           ID of the playbook you'd like to export.

options:
  -h, --help            show this help message and exit
  -s SRV, --srv SRV     Target MAS server to query (ip/domain:port format). (default: ['localhost:3001'])
  -o OUT, --out OUT     Target file to output. By default stdout. (default: None)
  -c, --clip            Put result on the clipboard. (default: 0)
  -u USERNAME, --username USERNAME
                        Name of the user whose privileges to be utilized (default: ['administrator'])
  -p PASSWORD, --password PASSWORD
                        Password of the user whose privileges to be utilized (default: ['administrator'])
  -m, --minify          Return minified json instead of pretty result. (default: 0)
  --http                Use HTTP instead of HTTPS. (default: 0)
  --no-strip            Don't strip values which are changing between installations, such as: apps, playbook id, dates. (default: 0)
  --no-cai              Don't replace custom applications with their bundle macros. (default: 0)

Internal use only!
```

---

## Script: `import_pb.py`

Used to import JSON files into the MAS software. By default it automatically resolves Bundle macros.

```bash
usage: import_pb.py [-h] [-s SRV] [-n NAME_PREFIX] [-u USERNAME] [-p PASSWORD] [--http] [--no-cai] [file_glob]

Import playbooks from JSON into OPSWAT MAS.

positional arguments:
  file_glob             Glob pattern to mark files to be uploaded. (default: *.json)

options:
  -h, --help            show this help message and exit
  -s SRV, --srv SRV     Target MAS server to query (ip/domain:port format). (default: ['localhost:3001'])
  -n NAME_PREFIX, --name-prefix NAME_PREFIX
                        Prefix target playbooks name with a given text. (default: None)
  -u USERNAME, --username USERNAME
                        Name of the user whose privileges to be utilized (default: ['administrator'])
  -p PASSWORD, --password PASSWORD
                        Password of the user whose privileges to be utilized (default: ['administrator'])
  --http                Use HTTP instead of HTTPS. (default: 0)
  --no-cai              Don't replace custom applications with their on-server equivalent. (default: 0)

Internal use only!
```

---

### Script: `export_run.py`

Used to export the outputs of a ran analysis, in order to be able to play with it on jmespath.

```bash
usage: export_run.py [-h] [-s SRV] [-u USERNAME] [-p PASSWORD] [-m] [-o OUT] [-c] [--http] incident_id

Exprt ran state incident.

positional arguments:
  incident_id           ID of the incident you'd like to export.

options:
  -h, --help            show this help message and exit
  -s SRV, --srv SRV     Target MAS server to query (ip/domain:port format). (default: ['localhost:3001'])
  -u USERNAME, --username USERNAME
                        Name of the user whose privileges to be utilized. (default: ['administrator'])
  -p PASSWORD, --password PASSWORD
                        Password of the user whose privileges to be utilized. (default: ['administrator'])
  -m, --minify          Return minified json instead of pretty result. (default: 0)
  -o OUT, --out OUT     Target file to output. By default stdout. (default: None)
  -c, --clip            Put result on the clipboard. (default: 0)
  --http                Use HTTP instead of HTTPs. (default: 0)

Internal use only!
```
