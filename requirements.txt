requests>=2.25.1
pyperclip>=1.8.2
argparse>=1.4.0
colorama>=0.4.4
jsonpatch>=1.32
