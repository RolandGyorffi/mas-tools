from pathlib import Path
from colorama import Fore, Style
import requests
import json
import argparse
import sys
import re
import urllib3

##############################################################################################
#                               G L O B A L    V A R I A B L E S                             #
##############################################################################################

global_headers = {'content-type': 'application/json'}
global_server = 'http://localhost:3001'
global_session = requests.Session()

##############################################################################################
#                               U S E R    D E F I N I T I O N S                             #
##############################################################################################

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def authenticate(username: str, password: str):
	login_payload = {'username': username, 'password': password}
	req_login = global_session.post(global_server + '/api/auth/login', headers=global_headers, data=json.dumps(login_payload))
	if req_login.status_code == 200: 
		global_headers['Authorization'] = f'Bearer {req_login.json()["access_token"]}'
	else:
		eprint(f'ERROR: Authentication unsuccesful: {req_login.status_code}')

def create_instance(app_type: str, payload):
    category = 'custom' if app_type is 'custom' else 'builtin'

    output = global_session.post(global_server + f'/api/management/apps/{category}/{app_type}/instances', data=json.dumps(payload), headers=global_headers)
    pass

##############################################################################################
#                                M A I N    E N T R Y P O I N T                              #
##############################################################################################

parser = argparse.ArgumentParser(description='Import instances from an exported json.', epilog='Internal use only!', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('file_glob', nargs='?', default='*.json', help='Glob pattern to mark files to be uploaded.')
parser.add_argument('-s', '--srv', nargs=1, default=['localhost:3001'], help='Target MAS server to query (ip/domain:port format).')
parser.add_argument('-n', '--name-prefix', nargs=1, help='Prefix target instance name with a given text.')
parser.add_argument('-u','--username', nargs=1, default=['administrator'], help='Name of the user whose privileges to be utilized')
parser.add_argument('-p','--password', nargs=1, default=['administrator'], help='Password of the user whose privileges to be utilized')
parser.add_argument('--http', action='count', default=0, help='Use HTTP instead of HTTPS.')

args = parser.parse_args()

# HTTP(S) related settings & build the connection string
if not args.srv[0].startswith('http'):
    global_server = ('http' if args.http else 'https') + '://' + args.srv[0]
elif args.srv[0].startswith('http') and not args.http:
    global_server = args.srv[0]
    print('URL contains HTTP directive, ignoring --http option.')

if global_server.startswith('https'): global_session.verify = False
if global_server.startswith('https'): urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# Log in to have a valid session
authenticate(args.username[0], args.password[0])

for file in Path('').glob(args.file_glob):
    body = file.read_text()

    # Load the JSON and apply the prefix when needed
    loaded = json.loads(body)
    
    for item in loaded['items']:
        if 'mdcore' in item['spec']['app']:
            create_instance('mdcore', item)
        elif 'mdcloud' in item['spec']['app']:
            create_instance('mdcloud', item)
        elif 'custom' in item['spec']['app']:
            create_instance('custom', item)
        else:
            eprint(f'[ERROR] "{item}" is an unknown app!')
