from pathlib import Path
from colorama import Fore, Style
import requests
import json
import argparse
import sys
import re
import urllib3

##############################################################################################
#                               G L O B A L    V A R I A B L E S                             #
##############################################################################################

global_headers = {'content-type': 'application/json'}
global_server = 'http://localhost:3001'
global_bundles = ''
global_session = requests.Session()

##############################################################################################
#                               U S E R    D E F I N I T I O N S                             #
##############################################################################################

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def init_resolver():
    global global_bundles

    req_bundles = global_session.get(global_server + '/api/management/bundles', headers=global_headers)
    global_bundles = json.loads(req_bundles.text)

def key_resolve_id(bundle_key, obj_key):
    global global_bundles

    items = global_bundles['items']

    for item in items:
        if bundle_key == item['id']:
            for obj in item['spec']['objects']:
                if obj_key == obj['spec']['key']:
                    return obj['spec']['id']

    return 'UNKNOWN'

def authenticate(username: str, password: str):
	login_payload = {'username': username, 'password': password}
	req_login = global_session.post(global_server + '/api/auth/login', headers=global_headers, data=json.dumps(login_payload))
	if req_login.status_code == 200: 
		global_headers['Authorization'] = f'Bearer {req_login.json()["access_token"]}'
	else:
		eprint(f'ERROR: Authentication unsuccesful: {req_login.status_code}')

##############################################################################################
#                                M A I N    E N T R Y P O I N T                              #
##############################################################################################

parser = argparse.ArgumentParser(description='Import playbooks from JSON into OPSWAT MAS.', epilog='Internal use only!', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('file_glob', nargs='?', default='*.json', help='Glob pattern to mark files to be uploaded.')
parser.add_argument('-s', '--srv', nargs=1, default=['localhost:3001'], help='Target MAS server to query (ip/domain:port format).')
parser.add_argument('-n', '--name-prefix', nargs=1, help='Prefix target playbooks name with a given text.')
parser.add_argument('-u','--username', nargs=1, default=['administrator'], help='Name of the user whose privileges to be utilized')
parser.add_argument('-p','--password', nargs=1, default=['administrator'], help='Password of the user whose privileges to be utilized')
parser.add_argument('--http', action='count', default=0, help='Use HTTP instead of HTTPS.')
parser.add_argument('--no-cai', action='count', default=0, help='Don\'t replace custom applications with their on-server equivalent.')

args = parser.parse_args()

# HTTP(S) related settings & build the connection string
if not args.srv[0].startswith('http'):
    global_server = ('http' if args.http else 'https') + '://' + args.srv[0]
elif args.srv[0].startswith('http') and not args.http:
    global_server = args.srv[0]
    print('URL contains HTTP directive, ignoring --http option.')

if global_server.startswith('https'): global_session.verify = False
if global_server.startswith('https'): urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# Log in to have a valid session
authenticate(args.username[0], args.password[0])

for file in Path('').glob(args.file_glob):
    body = file.read_text()

    # Replace CAIs with their real targets
    if not args.no_cai:
        init_resolver()

        temp_body = body

        for id_match in re.finditer(r'\/@(.*?)@', body):
            macro = id_match.group(1).split('/')

            bundle_key = ''
            obj_key = ''

            if len(macro) > 1:
                bundle_key = macro[0]
                obj_key = macro[1]
            else:
                obj_key = macro[0]

            obj_id = key_resolve_id(bundle_key, obj_key)
            temp_body = temp_body.replace(id_match.group(0), '/' + obj_id)

        body = temp_body

    # Load the JSON and apply the prefix when needed
    loaded = json.loads(body)
    if args.name_prefix:
        loaded['metadata']['name'] = args.name_prefix[0] + loaded['metadata']['name']
    
    pb_name = loaded['metadata']['name']
    body = json.dumps(loaded, separators=(',', ':'))
    
    req = global_session.post(global_server + '/api/management/playbooks', data=body, headers=global_headers)

    try:
        if 'error' in req.json():
            print('[' + Fore.RED + file.name + Style.RESET_ALL + ': MAS returned error message:\n')
            print(req.json()['error'])
        else:
            print('[' + Fore.GREEN + file.name + Style.RESET_ALL + ' was succesfully uploaded! (Playbook name: ' + pb_name + ')')
    except:
        print('[' + Fore.RED + file.name + Style.RESET_ALL + ': MAS returned REST error code: ' + str(req.status_code) + ': \n')
        print(req.text)
