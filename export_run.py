import pyperclip
import requests
import argparse
import urllib3
import json
import sys
import re

##############################################################################################
#                               G L O B A L    V A R I A B L E S                             #
##############################################################################################

global_headers = {'content-type': 'application/json'}
global_server = 'http://localhost:3001'
global_bundles = ''
global_session = requests.Session()

##############################################################################################
#                               U S E R    D E F I N I T I O N S                             #
##############################################################################################

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def authenticate(username: str, password: str):
	login_payload = {'username': username, 'password': password}
	req_login = global_session.post(global_server + '/api/auth/login', headers=global_headers, data=json.dumps(login_payload))
	if req_login.status_code == 200: 
		global_headers['Authorization'] = f'Bearer {req_login.json()["access_token"]}'
	else:
		eprint(f'ERROR: Authentication unsuccesful: {req_login.status_code}')

def get_output(incident_id, execution_id, node_key, output_key, id):
    output = global_session.get(global_server + f'/api/incidents/{incident_id}/executions/{execution_id}/nodes/{node_key}/outputs/{output_key}/{id}', headers=global_headers)
    jsn = json.loads(output.text)

    return jsn

def get_input(incident_id, execution_id, node_key, input_key, id):
    output = global_session.get(global_server + f'/api/incidents/{incident_id}/executions/{execution_id}/nodes/{node_key}/inputs/{input_key}/{id}', headers=global_headers)
    jsn = json.loads(output.text)

    return jsn

##############################################################################################
#                                M A I N    E N T R Y P O I N T                              #
##############################################################################################

parser = argparse.ArgumentParser(description='Export ran state incident.', epilog='Internal use only!', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('incident_id', nargs=1, help='ID of the incident you\'d like to export.')
parser.add_argument('-s', '--srv', nargs=1, default=['localhost:3001'], help='Target MAS server to query (ip/domain:port format).')
parser.add_argument('-u','--username', nargs=1, default=['administrator'], help='Name of the user whose privileges to be utilized.')
parser.add_argument('-p','--password', nargs=1, default=['administrator'], help='Password of the user whose privileges to be utilized.')
parser.add_argument('-m', '--minify', action='count', default=0, help='Return minified json instead of pretty result.')
parser.add_argument('-o', '--out', nargs=1, help='Target file to output. By default stdout.')
parser.add_argument('-c', '--clip', action='count', default=0, help='Put result on the clipboard.')
parser.add_argument('--http', action='count', default=0, help='Use HTTP instead of HTTPs.')

args = parser.parse_args()
incident_id = args.incident_id[0]

# HTTP(S) related settings & build the connection string
if not args.srv[0].startswith('http'):
    global_server = ('http' if args.http else 'https') + '://' + args.srv[0]
elif args.srv[0].startswith('http') and not args.http:
    global_server = args.srv[0]
    print('URL contains HTTP directive, ignoring --http option.')

if global_server.startswith('https'): global_session.verify = False
if global_server.startswith('https'): urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# Log in to have a valid session
authenticate(args.username[0], args.password[0])

req_pb = global_session.get(global_server + f'/api/incidents/{incident_id}/executions', headers=global_headers)
jsn = json.loads(req_pb.text)

execution_id = jsn['items'][0]['id']

req_pb = global_session.get(global_server + f'/api/incidents/{incident_id}/executions/{execution_id}', headers=global_headers)
jsn = json.loads(req_pb.text)
nodes = jsn['spec']['nodes']

for node_key in nodes:
    # Iterate all the possible output values & fill them
    if 'outputs' in nodes[node_key]:
        for node_outp in nodes[node_key]['outputs']:
            recovered_array = []
            for node_outp_id in nodes[node_key]['outputs'][node_outp]:
                output = get_output(incident_id, execution_id, node_key, node_outp, node_outp_id)
                recovered_array.append(output['spec']['value'])

            jsn['spec']['nodes'][node_key]['outputs'][node_outp] = recovered_array

    # Iterate all the possible input values & fill them
    if 'inputs' in nodes[node_key]:
        for node_inp in nodes[node_key]['inputs']:
            recovered_array = []
            for node_inp_id in nodes[node_key]['inputs'][node_inp]:
                input = get_input(incident_id, execution_id, node_key, node_inp, node_inp_id)
                recovered_array.append(input['spec']['value'])

            jsn['spec']['nodes'][node_key]['inputs'][node_inp] = recovered_array

# Either minify or prettify the output
if args.minify:
    payload = json.dumps(jsn, separators=(',', ':'))
else:
    payload = json.dumps(jsn, indent=4)

# Write to File or to STDOUT
if args.out:
    f = open(args.out[0], 'w')
    f.write(payload)
    f.close()
else:
    print(payload)

# Copy to clipboard if asked for
if args.clip:
    pyperclip.copy(payload)
